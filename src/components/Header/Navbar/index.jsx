import { pages } from "data/db.json";
import { arrayOf, bool, exact, string } from "prop-types";
import NavLink from "./NavLink";

const Navbar = ({ links }) => (
  <nav className="d-flex justify-content-between w-50">
    {links.map((data, index) => (
      <NavLink link={data} key={index} />
    ))}
  </nav>
);

Navbar.propTypes = {
  links: arrayOf(exact({ name: string, path: string, exact: bool })),
};

Navbar.defaultProps = {
  links: pages.map(({ name, route: { path, exact } }) => ({ name, path, exact })),
};

export default Navbar;

import PropType from "prop-types";
import { Button, Text } from "./view";

const SuccessBtn = ({ content }) => (
  <Button className="btn btn-success" to="register">
    <Text>{content}</Text>
  </Button>
);

SuccessBtn.propTypes = {
  content: PropType.string,
};

SuccessBtn.defaultProps = {
  content: "Register",
};

export default SuccessBtn;

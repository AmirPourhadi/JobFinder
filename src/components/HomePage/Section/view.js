import styled from "styled-components";

export const SectionTag = styled.section(({ background, height }) => ({
  height,
  background: `linear-gradient(${background[0]},${background[1]})`,
}));

export const Title = styled.h1({
  fontFamily: "mv boli",
  fontSize: 40,
  width: "80%",
});

export const Desc = styled.p({
  fontFamily: "ebrima",
  fontSize: 28,
});

export const Image = styled.img({
  width: "90%",
  borderRadius: "50px 0 50px 0",
  boxShadow: "10px 10px 10px rgba(0,0,0,0.4)",
});

import styled from "styled-components";

export const Main = styled.main({
  "& section:last-of-type": {
    boxShadow: "0 5px 10px -2px grey",
  },
  "& section:nth-of-type(2n)": {
    flexDirection: "row-reverse",
  },
});

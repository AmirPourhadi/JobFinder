import styled from "styled-components";

export const Main = styled.section({
  // backgroundColor: "#fff",
  fontFamily: "Arvo, serif",
  height: "calc(100vh - 75px - 40px)",
});

export const Gif = styled.div({
  background: "center 15px no-repeat url(https://cdn.dribbble.com/users/285475/screenshots/2083086/dribbble_1.gif)",
  height: 500,
});
